<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attach', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->default(0);
            $table->string('name',255)->nullable();
            $table->string('ext',10)->nullable();
            $table->integer('size')->nullable();
            $table->tinyInteger('save_type')->default(1);
            $table->string('qn_key',32)->nullable();
            $table->string('path',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attach');
    }
}
