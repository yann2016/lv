<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/4/22
 * Time: 下午11:17
 */

namespace App\Contracts;


use App\Models\AuthGroup;
use App\Models\Menu;

interface AuthContract
{
    public function getGroup(int $id): AuthGroup;

    public function getGroups(int $page, int $size): array;

    public function saveGroup(array $data): bool;

    public function updateGroup(array $data, AuthGroup $authGroup): bool;

    public function deleteGroup(int $id): bool;

    public function getRules(int $group_id = 0): array;

    public function deleteUserFromGroup(int $userId, int $groupId): bool;

    public function getMenu(int $id): Menu;

    public function getMenus(): array;

    public function saveMenu(array $data): bool;

    public function updateMenu(array $data, Menu $menu = null): bool;

    public function deleteMenu(int $id): bool;

}
