<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/5/8
 * Time: 下午9:17
 */

namespace App\Http\Controllers\Admin;


use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends BaseController
{
    public function index()
    {
        $data = Setting::findOrFail(1);
        return $this->buildSuccess($data);
    }

    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);
        $data = $request->all();
        if($setting->update($data)){
            return $this->buildSuccess();
        }else{
            return $this->buildError();
        }
    }
}
