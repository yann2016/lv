<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\AuthContract;
use Illuminate\Http\Request;

class AuthController extends BaseController
{
    //依赖注入
    public function __construct(Request $request, AuthContract $auth){
        $this->requset = $request;
        $this->auth = $auth;
    }

    /**
     * 权限组列表
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $page = $this->requset->get('page',1);
        $size = $this->requset->get('size',20);
        $result =  $this->auth->getGroups($page, $size);
        return $this->buildSuccess($result);
    }

    /**
     * 权限组信息
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $info = $this->auth->getGroup($id);
        return $this->buildSuccess($info);
    }

    /**
     * 添加权限组
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $data = $this->requset->all();
        if($this->auth->saveGroup($data)){
            return $this->buildSuccess();
        } else {
            return $this->buildError();
        }
    }


    /**
     * 更新权限组
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id)
    {
        $data = $this->requset->all();
        $model = $this->auth->getGroup($id);

        if($this->auth->updateGroup($data, $model)){
            return $this->buildSuccess();
        } else{
            return $this->buildError();
        }
    }

    /**
     * 删除权限组
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if($this->auth->deleteGroup($id)){
            return $this->buildSuccess();
        } else{
            return $this->buildError();
        }
    }

    public function getRules()
    {
        $group_id = $this->requset->get('group_id',0);
        $list = $this->auth->getRules($group_id);
        return $this->buildSuccess($list);
    }

    public function getMenus()
    {
        $list = $this->auth->getMenus();
        return $this->buildSuccess($list);
    }

    public function storeMenu()
    {
        $data = $this->requset->all();
        if($this->auth->saveMenu($data)){
            return $this->buildSuccess();
        } else {
            return $this->buildError();
        }
    }

    public function updateMenu($id)
    {
        $data = $this->requset->all();
        $model = $this->auth->getMenu($id);
        if($this->auth->updateMenu($data, $model)){
            return $this->buildSuccess();
        } else{
            return $this->buildError();
        }
    }

    public function destroyMenu($id)
    {
        if($this->auth->deleteMenu($id)){
            return $this->buildSuccess();
        } else {
            return $this->buildError();
        }
    }

    public function deleteUserFromGroup()
    {
        $uid = $this->requset->get('uid');
        $gid = $this->requset->get('gid');
        if(!$uid || !$gid)
            return $this->buildError(-1,'参数错误');

        if($this->auth->deleteUserFromGroup($uid, $gid)){
            return $this->buildSuccess();
        } else {
            return $this->buildError();
        }
    }
}
