<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/5/6
 * Time: 下午7:57
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected function buildSuccess($data = [], $msg = '操作成功')
    {
        $return = [
            'code' => 1,
            'msg' => $msg,
            'data' => $data
        ];
        return response()->json($return);
    }

    protected function buildError($code = 0, $msg = '操作失败')
    {
        $return = [
            'code' => $code,
            'msg' => $msg,
        ];
        return response()->json($return);
    }
}
