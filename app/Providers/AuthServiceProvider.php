<?php

namespace App\Providers;

use App\Services\AuthService;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
    public function register()
    {

        //使用bind绑定实例到接口以便依赖注入
        $this->app->bind('App\Contracts\AuthContract',function(){
            return new AuthService();
        });
    }
}
