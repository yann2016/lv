<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/4/23
 * Time: 下午10:06
 */

namespace App\Models;


class Menu extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'menu';

    protected $fillable = [
        'pid',
        'name',
        'route',
        'sort',
        'status',
    ];
}
