<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/4/23
 * Time: 下午10:09
 */

namespace App\Models;


class AuthGroup extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'auth_group';

    protected $fillable = [
        'name',
        'description',
        'status'
    ];
}
