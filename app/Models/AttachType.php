<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/5/8
 * Time: 下午11:31
 */

namespace App\Models;


class AttachType extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;
    protected $table = 'attach_type';
    protected $fillable = ['name'];
}
