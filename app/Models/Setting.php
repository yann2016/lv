<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/5/8
 * Time: 下午9:35
 */

namespace App\Models;


class Setting extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'setting';

    protected $fillable = ['title', 'logo', 'description', 'contact', 'phone', 'address', 'copyright', 'status'];

}
