<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/4/23
 * Time: 下午10:11
 */

namespace App\Models;


class AuthRule extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'auth_rule';

    protected $fillable = [
        'route',
        'group_id'
    ];
}
