<?php


namespace App\Models;


class Lottery extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;
    protected $table = 'lottery';

    protected $fillable = [
        'title',
        'openid',
        'pic',
        'album',
        'sponsor',
        'sponsor_appid',
        'sponsor_path',
        'prize_num',
        'content',
        'start_time',
        'end_time',
        'appoint',
        'sort',
        'status',
        'type',
        'open_type',
        'open_num',
        'label',
        'label_text',
    ];

}