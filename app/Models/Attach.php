<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/5/8
 * Time: 下午9:53
 */

namespace App\Models;


class Attach extends Base
{
    const CREATED_AT = null;
    const UPDATED_AT = null;
    const LOCALTYPE = 1;
    const QINIUTYPE = 2;
    protected $table = 'attach';

    protected $fillable = ['type','name','ext','size','save_type','qn_key','path'];
}
