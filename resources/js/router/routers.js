import Main from '@/components/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  title: { String|Number|Function }
 *         显示在侧边栏、面包屑和标签栏的文字
 *         使用'{{ 多语言字段 }}'形式结合多语言使用，例子看多语言的路由配置;
 *         可以传入一个回调函数，参数是当前路由对象，例子看动态路由和带参路由
 *  hideInBread: (false) 设为true后此级路由将不会出现在面包屑中，示例看QQ群路由配置
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面在切换标签后不会缓存，如果需要缓存，无需设置这个字段，而且需要设置页面组件name属性和路由配置的name一致
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */

export default [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: resolve => { require(['@/view/login/login.vue'], resolve) }
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          hideInMenu: true,
          title: '首页',
          notCache: true,
          icon: 'md-home'
        },
        component: resolve => { require(['@/view/single-page/home'], resolve) }
      }
    ]
  },
  {
    path: '/system',
    name: 'system',
    meta: {
      icon: 'md-desktop',
      title: '系统设置',
      access: ['admin.system.index']
    },
    component: Main,
    children: [
      {
        path: 'user',
        name: 'user',
        meta: {
          icon: 'ios-people',
          title: '管理员',
          access: ['admin.user.index']
        },
        component: resolve => { require(['@/view/system/user.vue'], resolve) }
      },
      {
        path: 'menu',
        name: 'menu_list',
        meta: {
          icon: 'md-menu',
          title: '菜单管理',
          access: ['admin.menu.index']
        },
        component: resolve => { require(['@/view/system/menu.vue'], resolve) }
      },
      {
        path: 'auth',
        name: 'auth',
        meta: {
          icon: 'md-warning',
          title: '权限管理',
          access: ['admin.auth.index']
        },
        component: resolve => { require(['@/view/system/auth.vue'], resolve) }
      },
      {
        path: 'setting',
        name: 'setting',
        meta: {
          icon: 'ios-settings',
          title: '基本设置',
          access: null
        },
        component: resolve => { require(['@/view/system/setting.vue'], resolve) }
      },
    ]
  },
  {
    path: '/lottery',
    name: 'lottery',
    meta: {
      icon: 'md-aperture',
      title: '微抽奖',
      access: ['admin.lottery.manage']
    },
    component: Main,
    children: [
      {
        path: 'lottery-list',
        name: 'lottery_list',
        meta: {
          icon: 'ios-paper-outline',
          title: '抽奖列表',
          access: ['admin.lottery.index']
        },
        component: resolve => { require(['@/view/lottery/list.vue'], resolve) }
      },
      {
        path: 'lottery-virtual',
        name: 'lottery_virtual',
        meta: {
          icon: 'md-person',
          title: '虚拟用户',
          access: ['admin.lottery.virtual']
        },
        component: resolve => { require(['@/view/lottery/virtual.vue'], resolve) }
      },
      {
        path: 'lottery-edit',
        name: 'lottery_edit',
        meta: {
          icon: 'ios-paper-outline',
          title: '编辑抽奖',
          hideInMenu: true
        },
        component: resolve => { require(['@/view/lottery/lottery-edit.vue'], resolve) }
      },
      {
        path: 'lucky-list',
        name: 'lucky_list',
        meta: {
          icon: 'ios-paper-outline',
          title: '中奖名单',
          hideInMenu: true
        },
        component: resolve => { require(['@/view/lottery/lucky-list.vue'], resolve) }
      }
    ]
  },
  {
    path: '/join',
    name: 'join',
    component: Main,
    meta: {
      hideInBread: true
    },
    children: [
      {
        path: 'join_page',
        name: 'join_page',
        meta: {
          icon: '_qq',
          title: 'QQ群'
        },
        component: resolve => { require(['@/view/join-page.vue'], resolve) }
      }
    ]
  },
  {
    path: '/message',
    name: 'message',
    component: Main,
    meta: {
      hideInBread: true,
      hideInMenu: true
    },
    children: [
      {
        path: 'message_page',
        name: 'message_page',
        meta: {
          icon: 'md-notifications',
          title: '消息中心'
        },
        component: resolve => { require(['@/view/single-page/message/index.vue'], resolve) }
      }
    ]
  },
  {
    path: '/components',
    name: 'components',
    meta: {
      icon: 'logo-buffer',
      title: '组件'
    },
    component: Main,
    children: [
      {
        path: 'tree_select_page',
        name: 'tree_select_page',
        meta: {
          icon: 'md-arrow-dropdown-circle',
          title: '树状下拉选择器'
        },
        component: resolve => { require(['@/view/components/tree-select/index.vue'], resolve) }
      },
      {
        path: 'count_to_page',
        name: 'count_to_page',
        meta: {
          icon: 'md-trending-up',
          title: '数字渐变'
        },
        component: resolve => { require(['@/view/components/count-to/count-to.vue'], resolve) }
      },
      {
        path: 'drag_list_page',
        name: 'drag_list_page',
        meta: {
          icon: 'ios-infinite',
          title: '拖拽列表'
        },
        component: resolve => { require(['@/view/components/drag-list/drag-list.vue'], resolve) }
      },
      {
        path: 'drag_drawer_page',
        name: 'drag_drawer_page',
        meta: {
          icon: 'md-list',
          title: '可拖拽抽屉'
        },
        component: resolve => { require(['@/view/components/drag-drawer'], resolve) }
      },
      {
        path: 'tree_table_page',
        name: 'tree_table_page',
        meta: {
          icon: 'md-git-branch',
          title: '树状表格'
        },
        component: resolve => { require(['@/view/components/tree-table/index.vue'], resolve) }
      },
      {
        path: 'cropper_page',
        name: 'cropper_page',
        meta: {
          icon: 'md-crop',
          title: '图片裁剪'
        },
        component: resolve => { require(['@/view/components/cropper/cropper.vue'], resolve) }

      },
      {
        path: 'tables_page',
        name: 'tables_page',
        meta: {
          icon: 'md-grid',
          title: '多功能表格'
        },
        component: resolve => { require(['@/view/components/tables/tables.vue'], resolve) }

      },
      {
        path: 'split_pane_page',
        name: 'split_pane_page',
        meta: {
          icon: 'md-pause',
          title: '分割窗口'
        },
        component: resolve => { require(['@/view/components/split-pane/split-pane.vue'], resolve) }

      },
      {
        path: 'markdown_page',
        name: 'markdown_page',
        meta: {
          icon: 'logo-markdown',
          title: 'Markdown编辑器'
        },
        component: resolve => { require(['@/view/components/markdown/markdown.vue'], resolve) }

      },
      {
        path: 'editor_page',
        name: 'editor_page',
        meta: {
          icon: 'ios-create',
          title: '富文本编辑器'
        },
        component: resolve => { require(['@/view/components/tree-table/index.vue'], resolve) }

      },
      {
        path: 'icons_page',
        name: 'icons_page',
        meta: {
          icon: '_bear',
          title: '自定义图标'
        },
        component: resolve => { require(['@/view/components/icons/icons.vue'], resolve) }
      }
    ]
  },
  {
    path: '/update',
    name: 'update',
    meta: {
      icon: 'md-cloud-upload',
      title: '数据上传'
    },
    component: Main,
    children: [
      {
        path: 'update_table_page',
        name: 'update_table_page',
        meta: {
          icon: 'ios-document',
          title: '上传Csv'
        },
        component: resolve => { require(['@/view/update/update-table.vue'], resolve) }
      },
      {
        path: 'update_paste_page',
        name: 'update_paste_page',
        meta: {
          icon: 'md-clipboard',
          title: '粘贴表格数据'
        },
        component: resolve => { require(['@/view/update/update-paste.vue'], resolve) }
      }
    ]
  },
  {
    path: '/excel',
    name: 'excel',
    meta: {
      icon: 'ios-stats',
      title: 'EXCEL导入导出'
    },
    component: Main,
    children: [
      {
        path: 'upload-excel',
        name: 'upload-excel',
        meta: {
          icon: 'md-add',
          title: '导入EXCEL'
        },
        component: resolve => { require(['@/view/excel/upload-excel.vue'], resolve) }
      },
      {
        path: 'export-excel',
        name: 'export-excel',
        meta: {
          icon: 'md-download',
          title: '导出EXCEL'
        },
        component: resolve => { require(['@/view/excel/export-excel.vue'], resolve) }
      }
    ]
  },
  {
    path: '/tools_methods',
    name: 'tools_methods',
    meta: {
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'tools_methods_page',
        name: 'tools_methods_page',
        meta: {
          icon: 'ios-hammer',
          title: '工具方法',
          beforeCloseName: 'before_close_normal'
        },
        component: resolve => { require(['@/view/tools-methods/tools-methods.vue'], resolve) }
      }
    ]
  },
  {
    path: '/i18n',
    name: 'i18n',
    meta: {
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'i18n_page',
        name: 'i18n_page',
        meta: {
          icon: 'md-planet',
          title: 'i18n - {{ i18n_page }}'
        },
        component: resolve => { require(['@/view/i18n/i18n-page.vue'], resolve) }
      }
    ]
  },
  {
    path: '/error_store',
    name: 'error_store',
    meta: {
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'error_store_page',
        name: 'error_store_page',
        meta: {
          icon: 'ios-bug',
          title: '错误收集'
        },
        component: resolve => { require(['@/view/error-store/error-store.vue'], resolve) }
      }
    ]
  },
  {
    path: '/directive',
    name: 'directive',
    meta: {
      hideInBread: true
    },
    component: Main,
    children: [
      {
        path: 'directive_page',
        name: 'directive_page',
        meta: {
          icon: 'ios-navigate',
          title: '指令'
        },
        component: resolve => { require(['@/view/directive/directive.vue'], resolve) }
      }
    ]
  },
  {
    path: '/multilevel',
    name: 'multilevel',
    meta: {
      icon: 'md-menu',
      title: '多级菜单'
    },
    component: Main,
    children: [
      {
        path: 'level_2_1',
        name: 'level_2_1',
        meta: {
          icon: 'md-funnel',
          title: '二级-1'
        },
        component: resolve => { require(['@/view/multilevel/level-2-1.vue'], resolve) }
      },
      {
        path: 'level_2_2',
        name: 'level_2_2',
        meta: {
          access: ['super_admin'],
          icon: 'md-funnel',
          showAlways: true,
          title: '二级-2'
        },
        component: parentView,
        children: [
          {
            path: 'level_2_2_1',
            name: 'level_2_2_1',
            meta: {
              icon: 'md-funnel',
              title: '三级'
            },
            component: resolve => { require(['@/view/multilevel/level-2-2/level-2-2-1.vue'], resolve) }
          },
          {
            path: 'level_2_2_2',
            name: 'level_2_2_2',
            meta: {
              icon: 'md-funnel',
              title: '三级'
            },
            component: resolve => { require(['@/view/multilevel/level-2-2/level-2-2-2.vue'], resolve) }
          }
        ]
      },
      {
        path: 'level_2_3',
        name: 'level_2_3',
        meta: {
          icon: 'md-funnel',
          title: '二级-3'
        },
        component: resolve => { require(['@/view/multilevel/level-2-3.vue'], resolve) }
      }
    ]
  },
  {
    path: '/argu',
    name: 'argu',
    meta: {
      hideInMenu: true
    },
    component: Main,
    children: [
      {
        path: 'params/:id',
        name: 'params',
        meta: {
          icon: 'md-flower',
          title: route => `{{ params }}-${route.params.id}`,
          notCache: true,
          beforeCloseName: 'before_close_normal'
        },
        component: resolve => { require(['@/view/argu-page/params.vue'], resolve) }
      },
      {
        path: 'query',
        name: 'query',
        meta: {
          icon: 'md-flower',
          title: route => `{{ query }}-${route.query.id}`,
          notCache: true
        },
        component: resolve => { require(['@/view/error-page/404.vue'], resolve) }
      }
    ]
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: resolve => { require(['@/view/error-page/404.vue'], resolve) }
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: resolve => { require(['@/view/error-page/404.vue'], resolve) }
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: resolve => { require(['@/view/error-page/404.vue'], resolve) }
  }
]
