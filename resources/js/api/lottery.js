import axios from '@/libs/api.request'

export const lottery = ({ type, page, size }) => {
  return axios.request({
    url: 'lottery',
    params: { type, page, size },
    method: 'get'
  })
}

export const getLottery = (id) => {
  return axios.request({
    url: 'lottery/' + id,
    method: 'get'
  })
}

export const addLottery = (data) => {
  return axios.request({
    url: 'lottery',
    data,
    method: 'post'
  })
}

export const editLottery = (id, data) => {
  return axios.request({
    url: 'lottery/' + id,
    data,
    method: 'put'
  })
}

export const delLottery = (data) => {
  return axios.request({
    url: 'lottery',
    data,
    method: 'delete'
  })
}

export const lucky = (data) => {
  return axios.request({
    url: 'Lottery/lucky',
    data,
    method: 'post'
  })
}

export const luckySave = (data) => {
  return axios.request({
    url: 'Lottery/luckySave',
    data,
    method: 'post'
  })
}

export const lotteryVirtual = (data) => {
  return axios.request({
    url: 'Lottery/virtual',
    data,
    method: 'post'
  })
}

export const virtualSave = (data) => {
  return axios.request({
    url: 'Lottery/virtualSave',
    data,
    method: 'post'
  })
}

export const virtualDel = (data) => {
  return axios.request({
    url: 'Lottery/virtualDel',
    data,
    method: 'post'
  })
}

export const virtualStatus = (data) => {
  return axios.request({
    url: 'Lottery/virtualStatus',
    data,
    method: 'post'
  })
}
