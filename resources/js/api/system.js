import axios from '@/libs/api.request'

export const getUsers = (data) => {
  return axios.request({
    url: 'user',
    params: data,
    method: 'get'
  })
}

export const getUser = (id) => {
  return axios.request({
    url: 'user/' + id,
    method: 'get'
  })
}

export const getGroupUsers = (gid, data) => {
  return axios.request({
    url: 'user/' + gid,
    params: data,
    method: 'get'
  })
}
export const addUser = (data) => {
  return axios.request({
    url: 'user',
    data,
    method: 'post'
  })
}

export const editUser = (id, data) => {
  return axios.request({
    url: 'user/' + id,
    data,
    method: 'put'
  })
}

export const delUser = (id) => {
  return axios.request({
    url: 'user/' + id,
    method: 'delete'
  })
}

export const delUserFormGroup = (data) => {
  return axios.request({
    url: 'auth/delUserFormGroup',
    data,
    method: 'post'
  })
}

export const getGroups = () => {
  return axios.request({
    url: 'auth',
    method: 'get'
  })
}

export const addGroup = (data) => {
  return axios.request({
    url: 'auth',
    data,
    method: 'post'
  })
}

export const editGroup = (id , data) => {
  return axios.request({
    url: 'auth/' + id,
    data,
    method: 'put'
  })
}

export const delGroup = (id) => {
  return axios.request({
    url: 'auth/' + id,
    method: 'delete'
  })
}

export const getRules = (data) => {
  return axios.request({
    url: 'rule',
    params: data,
    method: 'get'
  })
}

export const getMenus = () => {
  return axios.request({
    url: 'menu',
    method: 'get'
  })
}

export const addMenu = (data) => {
  return axios.request({
    url: 'menu',
    data,
    method: 'post'
  })
}

export const editMenu = (id, data) => {
  return axios.request({
    url: 'menu/' + id,
    data,
    method: 'put'
  })
}

export const delMenu = (id) => {
  return axios.request({
    url: 'menu/' + id,
    method: 'delete'
  })
}

export const setting = () => {
  return axios.request({
    url: 'setting',
    method: 'get'
  })
}

export const editSetting = (id, data) => {
  return axios.request({
    url: 'setting/' + id,
    data,
    method: 'put'
  })
}
