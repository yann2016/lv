import axios from '@/libs/api.request'

export const getQiniuToken = () => {
  return axios.request({
    url: 'qiniu',
    method: 'get'
  })
}

export const qiniuUpload = (data) => {
  return axios.request({
    url: 'qiniu',
    data,
    method: 'post'
  })
}

export const attach = ({ type, page, size }) => {
  return axios.request({
    url: 'attach',
    params: { type, page, size },
    method: 'get'
  })
}

export const delAttach = (ids) => {
  return axios.request({
    url: 'attach/delete',
    data: { ids },
    method: 'post'
  })
}

export const getType = () => {
  return axios.request({
    url: 'attach/type',
    method: 'get'
  })
}

export const addType = (data) => {
  return axios.request({
    url: 'attach/type',
    data,
    method: 'post'
  })
}

export const editType = (id, data) => {
  return axios.request({
    url: 'attach/type/' + id,
    data,
    method: 'put'
  })
}

export const delType = (id) => {
  return axios.request({
    url: 'attach/type/' + id,
    method: 'delete'
  })
}

export const moveAttachToType = (data) => {
  return axios.request({
    url: 'attach/changetype',
    data,
    method: 'post'
  })
}
