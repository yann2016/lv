<?php
/**
 * Created by PhpStorm.
 * User: yann
 * Date: 2019/4/17
 * Time: 下午8:36
 */

use Illuminate\Support\Facades\Redirect;

Route::get('/',function(){
    return Redirect::to('admin');
});

Route::get('/admin', function () {
    return view('admin');
});

//附件
Route::group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    Route::get('lottery','LotteryController@index')->name('admin.lottery.index');
    Route::get('lottery/{id}','LotteryController@show')->name('admin.lottery.show');
    Route::post('lottery','LotteryController@store')->name('admin.lottery.store');
    Route::put('lottery/{id}','LotteryController@update')->name('admin.lottery.update');
    Route::delete('lottery', 'LotteryController@destroy')->name('admin.lottery.destroy');
});

//附件
Route::group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    Route::get('attach','UploadController@index')->name('admin.attach.index');
    Route::post('attach','UploadController@store')->name('admin.attach.store');
    Route::post('attach/delete', 'UploadController@destroy')->name('admin.attach.destroy');
    Route::get('qiniu','UploadController@getQiniuToken')->name('admin.upload.qiniu');
    Route::post('qiniu','UploadController@qiniuStore')->name('admin.upload.qiniuStore');
    Route::get('attach/type','UploadController@attachType')->name('admin.attach.type');
    Route::post('attach/type','UploadController@typeStore')->name('admin.attach.typeStore');
    Route::put('attach/type/{id}','UploadController@typeUpdate')->name('admin.attach.typeUpdate');
    Route::delete('attach/type/{id}', 'UploadController@typeDestroy')->name('admin.upload.typeDestroy');
    Route::post('attach/changetype','UploadController@moveAttachToType')->name('admin.attach.changetype');

});

//基本设置
Route::group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    Route::get('setting','SettingController@index')->name('admin.setting.index');
    Route::put('setting/{id}', 'SettingController@update')->name('admin.setting.update');
});

//用户
Route::group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    Route::get('user','UserController@index')->name('admin.user.index');
    Route::post('user','UserController@store')->name('admin.user.store');
    Route::put('user/{id}', 'UserController@update')->name('admin.user.update');
    Route::delete('user/{id}', 'UserController@destroy')->name('admin.user.destroy');
    Route::get('user/{gid}','UserController@getUsers')->name('admin.user.get');

});

//权限
Route::group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    Route::get('auth','AuthController@index')->name('admin.auth.index');
    Route::post('auth','AuthController@store')->name('admin.auth.store');
    Route::put('auth/{id}','AuthController@update')->name('admin.auth.update');
    Route::delete('auth/{id}','AuthController@destroy')->name('admin.auth.destroy');
    Route::get('rule','AuthController@getRules')->name('admin.rule.index');
    Route::get('menu','AuthController@getMenus')->name('admin.menu.index');
    Route::post('menu','AuthController@storeMenu')->name('admin.menu.store');
    Route::put('menu/{id}','AuthController@updateMenu')->name('admin.menu.update');
    Route::delete('menu/{id}','AuthController@destroyMenu')->name('admin.menu.destroy');
    Route::post('auth/delUserFormGroup','AuthController@deleteUserFromGroup')->name('admin.auth.delUserFormGroup');
});

Route::group(['namespace'=>'Admin','prefix'=>'admin'],function (){
    //登录、注销
    Route::post('login','LoginController@login')->name('admin.login');
    Route::get('logout','LoginController@logout')->name('admin.logout');
    Route::post('refresh', 'LoginController@refresh');
    Route::post('me', 'LoginController@me');

});
